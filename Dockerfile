FROM askdjango/django_base:0.3
RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi

WORKDIR /code
COPY . /code/
RUN pip3 install -r reqs/common.txt

EXPOSE 8080
USER uwsgi

CMD ["uwsgi", "--http", "0.0.0.0:8080", "--harakiri", "600", "--wsgi-file", "/code/nia_crawler/wsgi.py"]

# 쉘> docker build -t nomade/encore:0.1 .
# 쉘> docker run -p 8080:8080 nomade/encore:0.1
