
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^news/$', views.news, name='news'),
    url(r'^recommend/$', views.recommend, name='recommend'),
    url(r'^indicator/$', views.indicator, name='indicator'),
    url(r'^test/$', views.test, name='test'),
    url(r'^oil/$', views.oil, name='oil'),
]
