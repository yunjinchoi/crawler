from django.shortcuts import render
from . import crawling

def news(request):
    crawling.crawler_kcif_finance_issue.start()
    return render(request, 'crawler/base.html')

def recommend(request):
    crawling.crawler_tb_kotra_prmz_mkt.start()
    return render(request, 'crawler/base.html')

def indicator(request):
    crawling.crawler_tb_indicator.start()
    return render(request, 'crawler/base.html')

def test(request):
    crawling.test.start()
    return render(request, 'crawler/base.html')

def oil(request):
    crawling.Tb_oil_price.start()
    return render(request, 'crawler/base.html')