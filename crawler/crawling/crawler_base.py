import pandas as pd
from datetime import date,datetime,timedelta
from sqlalchemy import create_engine

class Crawler_base(object):

    def __init__(self):
        self.engine = self.setting()

    def setting(self):
        params = {
            'user': 'azure@azure-mysql-db',
            'pass': 'dsc123!@#',
            'host': 'azure-mysql-db.mysql.database.azure.com',
            'port': '3306',
            'schema': 'encore_db?charset=utf8',
        }
        engine = create_engine('mysql+mysqldb://{user}:{pass}@{host}:{port}/{schema}'.format(**params), echo=False)
        return engine

    def check_lastupdate(self, tablename):
        df_dt=pd.read_sql('select crawling_dt from tb_lastupdate where table_name="'+tablename+'"', self.engine)
        now=datetime.today()
        nowday=date(now.year, now.month, now.day)
        return(df_dt['crawling_dt'][0]==nowday)

    