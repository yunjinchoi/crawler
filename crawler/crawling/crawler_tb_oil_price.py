from .crawler_base import Crawler_base
import pandas as pd
import requests
import time
import re
from bs4 import BeautifulSoup
from datetime import date,datetime,timedelta
from itertools import count
import numpy as np

class Tb_oil_price(Crawler_base) :

    def __init__(self):
        Crawler_base.__init__(self)
      
    def start(self):

        if(self.check_lastupdate('tb_oil_price')):
            print('crawling already finished')
            return

        today = date.today()
        startday=today - timedelta(days=6)

        ##수집할 날짜 설정
        temp_start=startday.strftime('%Y-%m-%d').split('-')
        temp_end=today.strftime('%Y-%m-%d').split('-')
        temp=temp_start+temp_end#배열 합치기

        url = 'http://www.opinet.co.kr/glopcoilSelect.do?TERM=D&STA_Y={0}&STA_M={1}&STA_D={2}&END_Y={3}&END_M={4}&END_D={5}&OILSRTCD1=001&OILSRTCD2=002&OILSRTCD3=003'.format(*temp)

        for retry in count(1):
            try:
                res= requests.get(url)
                break
            except requests.exceptions.ConnectionError as e:
                delay_seconds = retry * 2
                print('waiting', delay_seconds, 'seconds ...')
                time.sleep(delay_seconds)
                continue
        html = res.text
        soup = BeautifulSoup(html, 'html.parser')    
        col_name = []

        #table head 가져오기
        for temp in soup.select('.tbl_type10 th'):
            col_name.append(temp.text.strip())

        col_name = [w.lower() for w in col_name]
        if(col_name):
            col_name[0] = 'base_date'
        else:
            return
        ##내용 데이터 가져오기
        value = []
        for temp in soup.select('#tbody2 tr td'):
            value.append(temp.text.strip())
        value = [w.replace('-', '0') for w in value ]

        ##pandas 구조 만들기
        value_np=np.array(value).reshape(int(len(value)/len(col_name)),(len(col_name)))
        df=pd.DataFrame(columns=col_name,data=value_np)
        df['base_date'] = df['base_date'].apply(lambda x:datetime.strptime(x,'%y년%m월%d일').strftime('%Y-%m-%d'))
        
        
        crawling_day=today.strftime('%Y-%m-%d')
        df['last_update']=crawling_day
        for bd in df['base_date']:
            pd.read_sql('DELETE FROM tb_oil_price where base_date= %s',con=self.engine,params=[bd],chunksize=0)

        df.to_sql('tb_oil_price',con=self.engine, if_exists='append', index=False)

        print('{} 크롤링 완료\n'.format(crawling_day))

        conn=self.engine.connect()
        sql='UPDATE tb_lastupdate set crawling_dt=\''+crawling_day+'\' where table_name = \'tb_oil_price\';'
        u = conn.execute(sql)
        conn.close()
        print('tb_lastupdate저장 완료')

