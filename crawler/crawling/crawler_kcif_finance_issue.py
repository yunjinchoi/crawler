from .crawler_base import Crawler_base
import pandas as pd
import requests
import time
import re
from bs4 import BeautifulSoup
from datetime import date,datetime,timedelta
from itertools import count

class Kcif_finance_issue(Crawler_base) :

    def __init__(self):
        Crawler_base.__init__(self)

    def start(self):

        if(self.check_lastupdate('tb_fn')):
            print('crawling already finished')
            return

        urlList = pd.read_sql("SELECT url FROM tb_fn_copy ORDER BY write_dt LIMIT 10",con=self.engine)
        colnames = ['title', 'writer', 'write_dt', 'view', 'temp',]
        url = 'http://www.kcif.or.kr/front/board/boardList.do?intSection1=2&intSection2=4&intBoardID=1'
        html = requests.get(url)
        html = html.text
        soup = BeautifulSoup(html, 'html.parser')
        values = []
        for value in soup.select('div .board-list td'):
            values.append(value.text.strip())

        new_values = []
        for k in range(0,len(values),len(colnames)):
            j = k+ len(colnames)
            new_values.append(values[k:j])

        df = pd.DataFrame(data= new_values, columns=colnames)
        df = df.drop(axis=1, labels="temp")
        df = df.drop(axis=1, labels="view")


        temps = []
        for tag in soup.select('.board-list tbody .title a'):
            temps.append(tag.attrs['href'])
        temps = [re.findall(r"\d{5,6}",temp) for temp in temps]
        temp2 = []
        for i in temps:
            i = str(i).replace("[", "")
            i = str(i).replace("]", "")
            i = str(i).replace("'", "")
            temp2.append(i)

        urls = []
        for i in temp2:
            k = 'http://www.kcif.or.kr/front/board/boardView.do?intReportID={}'.format(i)
            urls.append(k)

        url = pd.DataFrame(urls,columns=['url',])
        new_df = df.join(url, how='left')

        new_df = new_df[~new_df.url.isin(urlList.url)]

        contents = []
        for url in new_df["url"]:
            html = requests.get(url)
            html = html.text
            soup_cont = BeautifulSoup(html, 'html.parser')
            for tag in soup_cont.select('div .board-view .cont'):
                contents.append(str(tag))


        contents = pd.DataFrame(contents,columns=['contents',])

        final_df = new_df.join(contents, how='left')
        final_df.to_sql('tb_fn_copy',con=self.engine, if_exists='append', index=False)
