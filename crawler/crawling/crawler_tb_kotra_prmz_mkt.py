from .crawler_base import Crawler_base
import pandas as pd
import requests
import time
import re
from bs4 import BeautifulSoup
from datetime import date,datetime,timedelta
from itertools import count
from html2text import html2text

class Tb_kotra_prmz_mkt(Crawler_base) :

    def __init__(self):
        Crawler_base.__init__(self)
      
    def start(self):

        if(self.check_lastupdate('tb_kotra_prmz_mkt')):
            print('crawling already finished')
            return

        baseUrl = 'http://news.kotra.or.kr'
        urlList = []
        dataIdxList = []
        titleList = []
        contentList = []
        contentListTemp = []
        nationList = []
        branchList = []
        w_dateList = []
        hscodeList = []

        for pageNum in range(1,10):
            url = 'http://news.kotra.or.kr/user/globalBbs/kotranews/25/globalBbsDataList.do?setIdx=254&page=' + str(pageNum)
            res = requests.get(url)
            html = res.text
            soup = BeautifulSoup(html, 'html.parser')
            for item in soup.select('table td a'):
                urlList.append(baseUrl + item.attrs['href'])


        before_dataidx_list = pd.read_sql("SELECT dataidx FROM tb_kotra_prmz_mkt order by w_date desc limit 1000",con=self.engine)


        for url in urlList:
            for retry in count(1):
                try:
                    res = requests.get(url)
                    break
                except requests.exceptions.ConnectionError as e:
                    delay_seconds = retry * 2
                    print('waiting', delay_seconds, 'seconds ...')
                    time.sleep(delay_seconds)
                    continue
            dataIdxList.append(url[url.index('dataIdx')+8:url.index('pageViewType')-1])
            html = res.text
            soup = BeautifulSoup(html, 'html.parser')
            values = []
            for item in soup.select('table td'):
                values.append(item.text)
            for item in soup.select('table .con'):
                contentListTemp.append(str(item))
            i = 0
            while i < len(values):
                titleList.append(values[i].strip())
                i += 1
                w_dateList.append(values[i].strip())
                i += 2
                nationList.append(values[i].strip())
                i += 1
                branchList.append(values[i].strip())
                i += 1
                hscodeTemp = ''
                for hscode in values[i].split('\n'):
                    hscode = hscode.replace('\t','')
                    hscode = hscode.replace(' ','')
                    if len(hscode) > 1:
                        hscodeTemp += hscode + ','
                hscodeList.append(hscodeTemp[:len(hscodeTemp)-1])
                break


        for content in contentListTemp:
            if content.find('<table')>0:
                content = content[:content.index('<table')] + content[content.index('</table')+8:]
            content = html2text(content)
            content = content.replace('**','')
            content = content.replace('\\','')
            contentList.append(content[:400] + '...')

        col_names = [
            'dataidx',
            'title',
            'content',
            'nation',
            'branch',
            'w_date',
            'hscode',
            'url'
        ]


        d = {
            'dataidx' : pd.Series(dataIdxList),
            'title' : pd.Series(titleList),
            'content' : pd.Series(contentList),
            'nation' : pd.Series(nationList),
            'branch' : pd.Series(branchList),
            'w_date' : pd.Series(w_dateList),
            'hscode' : pd.Series(hscodeList),
            'url' : pd.Series(urlList)
        }


        df = pd.DataFrame(data=d, columns=col_names)



        before_dataidx_list = before_dataidx_list['dataidx'].tolist()
        df = df[df['dataidx'].apply(lambda x: int(x) not in before_dataidx_list)]


        df.to_sql('tb_kotra_prmz_mkt', con=self.engine, if_exists='append', index=False)


        now = datetime.now()
        nowDate = now.strftime('%Y-%m-%d')
        conn=self.engine.connect()
        try:
            sql='UPDATE tb_lastupdate set crawling_dt=\''+nowDate+'\' where table_name = \'tb_kotra_prmz_mkt\';'
            u = conn.execute(sql)
        except:
            print("")
        conn.close()
