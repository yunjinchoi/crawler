from .crawler_base import Crawler_base
import pandas as pd
import requests
import time
import re
from bs4 import BeautifulSoup
from datetime import date,datetime,timedelta
from itertools import count

class Tb_indicator(Crawler_base) :

    def __init__(self):
        Crawler_base.__init__(self)

    def start(self):
        
        if (self.check_lastupdate('tb_indicator')):
            print('크롤링이미완료')
            return

        #db에서 indicator list가져오기
        dbNameList = pd.read_sql("SELECT topic_enm FROM tb_topic where main_cd = 'N'",con=self.engine)
        dbNameList = dbNameList['topic_enm'].tolist()
        dbNameList.remove('Credit Rating')


        #기존 db리스트와 웹페이지의 문자가 다르므로 비교하기위해 모두 upper
        dbNameList_upper=[]
        for a in dbNameList:
            dbNameList_upper.append(a.upper())



        #dbNameList에 있는 indicator만 크롤링할 예정임
        baseURL='https://tradingeconomics.com'
        res = requests.get(baseURL+'/indicators')
        time.sleep(1)
        html = res.text
        soup = BeautifulSoup(html, 'html.parser')
        indicatorNameList=[] #나중에 웹페이지상의 indicator의 변화를 대비하여 NameList를 둠.
        indicatorLinkList=[]
        for item in soup.select("div ul.list-unstyled li.list-group-item a"):
            if(item.text.upper() in dbNameList_upper):
                indicatorNameList.append(item.text.strip())
                indicatorLinkList.append(item.attrs['href'])


        ##중복되는 indicator가 있으므로 중복제거
        indi_col=['name','link']
        df_indi=pd.DataFrame(columns=indi_col)
        df_indi['name']=indicatorNameList
        df_indi['link']=indicatorLinkList

        indi_drop=df_indi.drop_duplicates(indi_col)

        indicatorNameList=indi_drop['name'].tolist()
        indicatorLinkList=indi_drop['link'].tolist()

        
        ##
        col_names = ['country_ename',
        'value',
        'last_update',
        'prv_value',
        'max_value',
        'min_value',
        'unit',
        'cycle',
        'temp'
        ]

        ##크롤링 시작
        now = datetime.now()
        nowDate = now.strftime('%Y-%m-%d')
        counts=0

        pd.read_sql('DELETE FROM tb_indicator where create_dt=%s',con=self.engine,params=[nowDate],chunksize=0)

        for linki in range(0,len(indicatorLinkList)):
            url = baseURL+indicatorLinkList[linki]
            for retry in count(1):
                try:
                    res = requests.get(url)
                    counts+=1
                    break
                except requests.exceptions.ConnectionError as e:
                    delay_seconds = retry * 2
                    time.sleep(delay_seconds)
                    continue
            html = res.text
            soup = BeautifulSoup(html, 'html.parser')   
            values = []

        ##각 지수별 값 가져오기
            for temp in soup.select('.table-responsive td'):
                values.append(temp.text.strip()) # year month value | year value ##테이블값


            new_data = []
            for k in range(0,len(values),len(col_names)):
                i = k + len(col_names)
                new_data.append(values[k:i])

            df = pd.DataFrame(data=new_data, columns=col_names).fillna('')
            df.drop("temp", axis=1,inplace=True)
            df.insert(0, "topic_enm", '{}'.format(indicatorNameList[linki].upper()))
            temp1 = pd.read_sql("SELECT upper(topic_enm) as topic_enm, topic_cd FROM tb_topic where main_cd='N' and upper(topic_enm) = %s",params=[indicatorNameList[linki].upper()],con=self.engine)

            new_df_merge = pd.merge(df,temp1, how='left')
            new_df = new_df_merge.drop('topic_enm', axis=1)

            temp2 = pd.read_sql("SELECT country_code, country_ename  FROM tb_un_country", con=self.engine)
            final_df = pd.merge(new_df, temp2, how='left', on="country_ename")
            final_df['prv_value'] = pd.to_numeric(final_df['prv_value'])
            final_df['max_value'] = pd.to_numeric(final_df['max_value'])
            final_df['min_value'] = pd.to_numeric(final_df['min_value'])
            final_df['value'] = pd.to_numeric(final_df['value'])

            cols=final_df.columns.tolist()
            cols=cols[-2:]+cols[:-2]

            final_df=final_df[cols]
            final_df.insert(10,'create_dt',nowDate)

            last_up=[]
            for aa in final_df['last_update']:
                last_up.append(datetime.strptime(aa,'%b/%y').strftime('%Y%m'))

            final_df = final_df.drop('last_update', axis=1)
            final_df.insert(4,'last_update',last_up)


            final_df.to_sql('tb_indicator',con=self.engine,if_exists='append' ,index=False) 


            time.sleep(1)


        ##update last update
        if(counts==len(indicatorLinkList)):
            conn=engine.connect()
            sql='UPDATE tb_lastupdate set crawling_dt=\''+nowDate+'\' where table_name = \'tb_indicator\';'
            u = conn.execute(sql)
            conn.close()

