from . import crawler_tb_oil_price, crawler_tb_kotra_prmz_mkt, crawler_tb_indicator, crawler_kcif_finance_issue

crawler_kcif_finance_issue = crawler_kcif_finance_issue.Kcif_finance_issue()
crawler_tb_indicator = crawler_tb_indicator.Tb_indicator()
crawler_tb_kotra_prmz_mkt = crawler_tb_kotra_prmz_mkt.Tb_kotra_prmz_mkt()
crawler_tb_oil_price = crawler_tb_oil_price.Tb_oil_price()
